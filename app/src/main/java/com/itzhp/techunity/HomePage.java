package com.itzhp.techunity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.database.Cursor;
import android.os.Bundle;
import android.widget.TextView;

import com.itzhp.techunity.db.DatabaseHelper;

import java.util.ArrayList;
import java.util.Calendar;

public class HomePage extends AppCompatActivity {
    DatabaseHelper myDb;
    ArrayList<String> name=new ArrayList<>();
    ArrayList<String> age=new ArrayList<>();
    ArrayList<String> gender=new ArrayList<>();
    private RecyclerView recyclerView;
    private Useradapter useradapter;
    private  RecyclerView.LayoutManager layoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        recyclerView=findViewById(R.id.rec);

        myDb = new DatabaseHelper(this);
        Cursor cursor = myDb.getUser();
        name.clear();gender.clear();age.clear();
        if (cursor.getCount() != 0) {
            while (cursor.moveToNext()) {
                name.add(cursor.getString(1));
                gender.add(cursor.getString(4));
                String[] arraydate =cursor.getString(3).split("-");
                age.add(String.valueOf(getAge(Integer.parseInt(arraydate[0]),Integer.parseInt(arraydate[1]),Integer.parseInt(arraydate[2]))));

            }
        }
        layoutManager=new LinearLayoutManager(this);
        try {
            recyclerView.setHasFixedSize(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        recyclerView.setLayoutManager(layoutManager);
        useradapter=new Useradapter(name,age,gender);
        recyclerView.setAdapter(useradapter);
         }
    private int getAge(int year, int month, int day) {
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();
        dob.set(year, month, day);
        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }
        if(age<0){
            age=0;
        }
        return age;
    }
}