package com.itzhp.techunity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.itzhp.techunity.db.DatabaseHelper;

import java.util.Calendar;
import java.util.regex.Pattern;


public class Signup extends AppCompatActivity {
    TextInputEditText fname,lname,email,pass,dob,ageedit;
    DatabaseHelper myDb;
    RadioGroup rg;
    RadioButton rb;
    String gender,age;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        fname=findViewById(R.id.firstname);
        lname=findViewById(R.id.lastnameid);
        email=findViewById(R.id.email);
        dob=findViewById(R.id.dob);
        pass=findViewById(R.id.password);
        ageedit=findViewById(R.id.age);
        rg=findViewById(R.id.radioGroup);
        myDb = new DatabaseHelper(this);
    }

    public void Signup(View view) {
        Pattern pattern;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        if( email.getText().toString().isEmpty() || pass.getText().toString().isEmpty()|| fname.getText().toString().isEmpty()||dob.getText().toString().isEmpty()||!Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()||!pattern.matcher(pass.getText().toString()).matches() ){
           if(email.getText().toString().isEmpty()){
               email.setError("Required");
           }else if(!Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()){
               email.setError("Email Isn't Valid");
           }
            if(pass.getText().toString().isEmpty()){
                pass.setError("Required");
            }else if(!pattern.matcher(pass.getText().toString()).matches() ){
                pass.setError("Password Isn't Valid (Min 1 Character , 1 special Character and 1 Numeric)");
            }
            if(fname.getText().toString().isEmpty()){
                fname.setError("Required");
            }
            if(dob.getText().toString().isEmpty()){
                dob.setError("Required");
            }

        }else {
            int sid=rg.getCheckedRadioButtonId();
            rb=(RadioButton)findViewById(sid);
            gender=rb.getText().toString();
            boolean check=myDb.insertuserData(fname.getText().toString() +" "+ lname.getText().toString(),pass.getText().toString() ,dob.getText().toString(),gender,email.getText().toString());
            if (check){
                Toast.makeText(this, "Successfully Created Account", Toast.LENGTH_LONG).show();
                startActivity(new Intent(Signup.this,Login.class));
            }
        }
    }

    public void signin(View view) {
        startActivity(new Intent(Signup.this,Login.class));
    }
    private int getAge(int year, int month, int day) {
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();
        dob.set(year, month, day);
        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }
        if(age<0){
            age=0;
        }
        return age;
    }
    public void dob(View view) {
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog mdiDialog = new DatePickerDialog(Signup.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String mm, dd;
                monthOfYear = monthOfYear + 01;
                if (monthOfYear < 10) {

                    mm = "0" + monthOfYear;
                } else {
                    mm = String.valueOf(monthOfYear);
                }
                if (dayOfMonth < 10) {

                    dd = "0" + dayOfMonth;
                } else {
                    dd = String.valueOf(dayOfMonth);
                }
                age=String.valueOf(getAge(year,monthOfYear,dayOfMonth));
                ageedit.setText(age);
                dob.setText(year + "-" + mm + "-" + dd);

            }
        }, mYear, mMonth, mDay);
        mdiDialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);
        mdiDialog.show();
    }
}