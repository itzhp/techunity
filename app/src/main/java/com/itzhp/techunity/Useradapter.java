package com.itzhp.techunity;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;

public class Useradapter extends RecyclerView.Adapter<Useradapter.Imagevh> {
    ArrayList<String> name=new ArrayList<>();
    ArrayList<String> age=new ArrayList<>();
    ArrayList<String> gender=new ArrayList<>();
    public Context context;
    public Useradapter(ArrayList<String> user_name,ArrayList<String> age,ArrayList<String> gender){
        this.name=user_name;
        this.age=age;
        this.gender=gender;
    }

    @NonNull
    @Override
    public Imagevh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.userslist,parent,false);
        Imagevh imagevh=new Imagevh(view);

        context=parent.getContext();
        return imagevh;
    }

    @Override
    public void onBindViewHolder(@NonNull final Imagevh holder, final int position) {
        holder.name.setText("Name   : "+name.get(position));
        holder.age.setText("Age       : "+age.get(position));
        holder.gender.setText("Gender : "+gender.get(position));


    }

    @Override
    public int getItemCount() {
        return name.size();
    }

    public  static class Imagevh extends RecyclerView.ViewHolder{

        TextView name,gender,age;
        public Imagevh(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.name);
            gender=itemView.findViewById(R.id.gender);
            age=itemView.findViewById(R.id.age);

        }
    }
}

