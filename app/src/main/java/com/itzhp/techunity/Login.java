package com.itzhp.techunity;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.itzhp.techunity.db.DatabaseHelper;


public class Login extends AppCompatActivity {
    TextInputEditText phno,pass;
    DatabaseHelper myDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        phno=(TextInputEditText) findViewById(R.id.pno);
        pass=(TextInputEditText)findViewById(R.id.pass);
        myDb = new DatabaseHelper(this);

    }

    public void signin(View view) {
        final String email = phno.getText().toString().trim();
        final String password = pass.getText().toString().trim();
        if(!email.isEmpty()|| !password.isEmpty()){
            Cursor cursor = myDb.login(email,password);
            if (cursor.getCount() != 0) {
                Intent i=new Intent(Login.this, HomePage.class);
                startActivity(i);

            } else {
                Toast.makeText(Login.this, "You are not a valid user", Toast.LENGTH_LONG).show();
            }
            myDb.close();
        }else {
            phno.setError("Required");
            pass.setError("Required");
        }
    }

    public void signup(View view) {
        startActivity(new Intent(Login.this,Signup.class));
    }
}