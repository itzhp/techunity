package com.itzhp.techunity.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Hari on 28/04/2021.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "techunity.db";
    public static final String USER_TABLE_NAME = "user";

    public static final String COL1 = "name";
    public static final String COL_2 = "password";
    public static final String COL_3 = "dob";
    public static final String COL_5 = "gender";
    public static final String COL_6 = "email";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + USER_TABLE_NAME + "(id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT,password TEXT,dob TEXT,gender TEXT,email TEXT)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + USER_TABLE_NAME);
        onCreate(db);
    }
    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            MessageDigest digest = MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public boolean insertuserData(String name,String password,String dob,String gender,String mail) {
        long result = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL1, name);
        contentValues.put(COL_2, md5(password));
        contentValues.put(COL_3, dob);
        contentValues.put(COL_5, gender);
        contentValues.put(COL_6, mail);
        result = db.insert(USER_TABLE_NAME, null, contentValues);
        return result != -1;
    }

    public Cursor login(String email,String password) {
        SQLiteDatabase dbh = this.getWritableDatabase();
        Cursor myCursor = dbh.rawQuery("SELECT * FROM "+USER_TABLE_NAME+" WHERE "+COL_6+"= '"+email+"' AND "+COL_2+"= '"+md5(password)+"'", null);
        return myCursor;
    }

    public Cursor getUser() {
        SQLiteDatabase dbh = this.getWritableDatabase();
        Cursor myCursor = dbh.rawQuery("SELECT * FROM "+USER_TABLE_NAME, null);
        return myCursor;
    }
   }
